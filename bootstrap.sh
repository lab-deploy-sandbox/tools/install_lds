#set -e
#
function packages_install(){
 sudo yum install -y epel-release
 sudo yum install -y ansible

}

packages_install

sudo ansible-playbook install_conda.yml
