# install LDS and Conda

This example will install using Vagrant. For a local install run bootstrap.sh

A setup user called varget will be generated. The actual user where LDS installs will be iocuser

In this install iocuser will have no password, to access iocuser, from vagrant:
  sudo -u iocuser /bin/bash

To setup locally without vagrant, copy the repository and run this script:
bash ./bootstrap.sh

See this page for more details:
https://confluence.esss.lu.se/display/IS/Installing+LDS+Conda+E3+on+servers+outside+ESS

